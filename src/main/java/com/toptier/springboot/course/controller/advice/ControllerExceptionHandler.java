package com.toptier.springboot.course.controller.advice;

import com.toptier.springboot.course.dto.advice.BusinessException;
import com.toptier.springboot.course.dto.advice.ErrorMessage;
import com.toptier.springboot.course.dto.advice.UnAuthException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.time.OffsetDateTime;

@RestControllerAdvice
public class ControllerExceptionHandler {

  @ExceptionHandler(RuntimeException.class)
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public ErrorMessage globalException(RuntimeException ex) {
    return new ErrorMessage(
            500,
            ex.getMessage(),
            OffsetDateTime.now()
    );
  }

  @ExceptionHandler(BusinessException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ErrorMessage businessException(BusinessException ex) {
    return new ErrorMessage(
            ex.getStatus(),
            ex.getMessage(),
            OffsetDateTime.now()
    );
  }

  @ExceptionHandler(UnAuthException.class)
  @ResponseStatus(HttpStatus.UNAUTHORIZED)
  public ErrorMessage unAuthException(UnAuthException ex) {
    return new ErrorMessage(
            401,
            ex.getMessage(),
            OffsetDateTime.now()
    );
  }

  // API Not found Exception
  @ExceptionHandler(NoHandlerFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public ErrorMessage apiNotFoundException(NoHandlerFoundException ex) {
    return new ErrorMessage(
            404,
            ex.getMessage(),
            OffsetDateTime.now()
    );
  }

}
