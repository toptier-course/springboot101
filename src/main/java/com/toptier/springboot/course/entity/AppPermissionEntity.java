package com.toptier.springboot.course.entity;

import lombok.Data;

@Data
public class AppPermissionEntity {
  private String appName;
  private String appId;
}
