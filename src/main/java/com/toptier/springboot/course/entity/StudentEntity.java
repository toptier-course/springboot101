package com.toptier.springboot.course.entity;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class StudentEntity {

  private String code;
  private String name;
  private String lastName;
  private String level; // Enum
  private String dateOfBirth;
  private String sex;
  private String email;
  private Integer age;
}
