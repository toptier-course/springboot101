package com.toptier.springboot.course.config;

import com.toptier.springboot.course.controller.interceptor.AppIdInterceptor;
import com.toptier.springboot.course.controller.interceptor.HelloInterceptor;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@RequiredArgsConstructor
public class WebMvcConfig implements WebMvcConfigurer {

  final HelloInterceptor helloInterceptor;
  final AppIdInterceptor appIdInterceptor;

  @Override
  public void addInterceptors(InterceptorRegistry registry) {

//    registry.addInterceptor(helloInterceptor)
//            .addPathPatterns("/api/v1/students");
//
//    registry.addInterceptor(appIdInterceptor);

  }
}
