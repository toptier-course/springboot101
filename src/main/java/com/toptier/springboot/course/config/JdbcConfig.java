package com.toptier.springboot.course.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.util.Properties;

@Log
@Configuration
@RequiredArgsConstructor
public class JdbcConfig {

  @Value("${config.mysql.url}")
  String URL;

  final ConfigMySql CONFIG_MYSQL;

  @Bean
  public NamedParameterJdbcTemplate namedJdbcTemplate(DataSource dataSource) {
    var namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    namedParameterJdbcTemplate.getJdbcTemplate().setFetchSize(1000);
    return namedParameterJdbcTemplate;
  }

  @Bean
  public JdbcTemplate jdbcTemplate(DataSource dataSource) {
    var jdbcTemplate = new JdbcTemplate(dataSource);
    jdbcTemplate.setFetchSize(100);
    return jdbcTemplate;
  }

  @Bean
  public DataSource oracleDataSource() {
    log.info("Database URL: " + URL);
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
    dataSource.setUrl("jdbc:mysql://" + URL + "/" + CONFIG_MYSQL.SCHEMA);
    dataSource.setUsername(CONFIG_MYSQL.USERNAME);
    dataSource.setPassword(CONFIG_MYSQL.PASSWORD);
    dataSource.setSchema(CONFIG_MYSQL.SCHEMA);
    Properties properties = new Properties();
    properties.setProperty("validationQuery", "SELECT CURDATE()");
    properties.setProperty("useSSL", "false");
    dataSource.setConnectionProperties(properties);
    return dataSource;
  }

  @lombok.Value
  @ConstructorBinding
  @ConfigurationProperties("config.mysql")
  private static class ConfigMySql {
    String URL;
    String USERNAME;
    String PASSWORD;
    String SCHEMA;
  }
}
